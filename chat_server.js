// Require the packages we will use:
var http = require("http"),
    socketio = require("socket.io"),
    fs = require("fs");
 
// Listen for HTTP connections.  This is essentially a miniature static file server that only serves our one file, client.html:
var app = http.createServer(function(req, resp){
// This callback runs when a new connection is made to our HTTP server.
fs.readFile("client.html", function(err, data){
// This callback runs when the chat.html file has been read from the filesystem.
	if(err) return resp.writeHead(500);
	resp.writeHead(200);
	resp.end(data);
	});
});
app.listen(3456);
var usernames ={};
var rooms ={};
var rooms2 ={};
var privaterooms ={};
var owner ={};                         //owner{room:username}
var blacklist ={};                     //blacklist{room: username}
var passwords =[];                     //array that stores passwords   

var io = socketio.listen(app);
io.sockets.on("connection", function(socket){
// This callback runs when a new Socket.IO connection is established.
// when the client emits 'adduser', this function listens and executes

socket.on('adduser', function(username){
	
	usrjudge(username);
	
	io.sockets.connected[username] = socket.id;
	socket.room = 'lobby';
	rooms2[username] = socket.room
	socket.join('lobby');
	console.log("new user: "+socket.username);
	io.sockets.emit("currentUsers",usernames);
	socket.emit("message_to_client",{message: "Welcome" + " " + socket.username });
});

function usrjudge(username){
	if(username === null){
		socket.username = 'guest';
	}else{
		
		socket.username = username;
		usernames[username] = username;
	}

}
// This callback runs when users create a new room
socket.on('create', function(newroom) {
	socket.leave(socket.room);
	socket.join(newroom);
	socket.room = newroom;
	rooms[newroom] = newroom;
	rooms2[socket.username] = socket.room;
	owner[socket.room] = socket.username;
	
	io.sockets.emit("displayRooms",rooms);
	socket.emit("currentRooms",newroom);
	io.sockets.emit('alert_join', {message: socket.username +" has joined"+ " " +newroom});
});
//This callback runs when users join a specified room
socket.on('join', function(joinroom) {
	
	if (blacklist[socket.username] === joinroom) {
		console.log("You are banned from room"+ joinroom);		
	}else{
	socket.leave(socket.room);
	socket.join(joinroom);
	socket.room = joinroom;
	rooms2[socket.username] = socket.room;
	
	io.sockets.emit("displayRooms",rooms);
	socket.emit("currentRooms",joinroom);
	io.sockets.emit('alert_join', {message: socket.username +" has joined"+ " " +joinroom});
	}
});

socket.on('pm', function(proom, password) {
	socket.leave(socket.room);
	p =password;
	socket.password = password;
	socket.join(proom);
	socket.room = proom;
	privaterooms[proom] = proom;
	rooms2[socket.username] = socket.room
	owner[socket.room] = socket.username;
	
	passwords.push(password); 
	io.sockets.emit("privateRoomsAval",privaterooms);
	io.sockets.emit('alert_join', {message: socket.username +" has created"+ " " +proom});
});

socket.on('pm2', function(proom, password2) {
	for (var i =0; i<passwords.length; i++){
	if((passwords[i] === password2)){
		socket.leave(socket.room);
		socket.join(proom);
		socket.room = proom;
		privaterooms[proom] = proom;
		rooms2[socket.username] = socket.room
		io.sockets.emit('alert_join', {message: socket.username +" has joined"+ " " +proom});
	}else{
	socket.emit("message_to_client",{message: "When trying to access" + " " + "<"+proom+">" + " "+ "you put in the wrong password! Try Again!"});
	}
	}
});

//This callback runs when users try to start a private chat to someone in the chatting room
socket.on('whisp', function(userw, messw){
	if(rooms2[socket.username] === rooms2[userw]){
	socket.emit("message_to_client",{message: "whisper sent to" + " " + userw + " " + ":" + " " +"<"+messw+">"});
	io.to(io.sockets.connected[userw]).emit("message_to_client",{message: socket.username + " " + "whispers" + " " + messw}) 
	}else{
	socket.emit("message_to_client",{message: userw + " " + "is not in your room"});
	}
});

//This callback runs when creator of chat room wants to kick someone temporarily
socket.on('kick_temp',function(user_kick){
	console.log("current username: "+socket.username);
	console.log("creator username: "+owner[socket.room]);
	
	console.log("this user's room:"+ rooms2[user_kick]);
	console.log("this room is: "+socket.room);
	
	if (socket.username !== owner[socket.room]) {

		console.log("you are not the creator");
		
	}else if (rooms2[user_kick] !== socket.room) {
		//user not in the room
		
		console.log("user " + user_kick + " is not in this room");
		
	}else{
	io.sockets.emit('Kick',user_kick);		//broadcast to every user.

	}
});
//real kick someone out to lobby.
socket.on('KickReal', function() {
	console.log("user "+socket.username+" has been kicked out temp from " + socket.room);
	socket.leave(socket.room);
	socket.room = 'lobby';
	rooms2[socket.username] = socket.room;
	socket.join('lobby');
});

//This callback runs when the creator of the chatting room wants to kick someone permenantly
socket.on('kickPerm',function(usr_to_kick){
	console.log("current username: "+socket.username);
	console.log("creator username: "+owner[socket.room]);
	console.log("this user's room:"+ rooms2[usr_to_kick]);
	console.log("this room is: "+socket.room);
	
	if (socket.username !== owner[socket.room]) {
		
		console.log("you are not the creator");
		
	}else if (rooms2[usr_to_kick] !== socket.room) {
		//user not in the room
		console.log("user " + usr_to_kick + " is not in this room");
		
	}else{
	io.sockets.emit('KickP',usr_to_kick);

	}
});

//real kick out to lobby and put it in a blacklist.
socket.on('KickRealP', function() {
	blacklist[socket.username] = socket.room;
	console.log("user "+socket.username+" has been kicked out perminantly from " + socket.room);
	socket.leave(socket.room);
	socket.room = 'lobby';
	rooms2[socket.username] = socket.room;
	socket.join('lobby');
});

// This callback runs when the server receives a new message from the client.
socket.on('message_to_server', function(data) {

	console.log(socket.username +" " + ":" + " " +data["message"]); // log it to the Node.JS output
	io.sockets.to(socket.room).emit("message_to_client",{message: "<"+socket.room+">" + " " + socket.username + ":" + " " + " "+ data["message"] }) // broadcast the message to other users
});

//This callback runs when the user shut down the chatting room window.
socket.on('disconnect', function(){
	delete usernames[socket.username];
	io.sockets.emit("currentUsers",usernames);
	console.log(socket.username + " " + "has left");
});

});
